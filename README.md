# FreeBSD 13 X230

Configuration files with security enhancements for FreeBSD 13 on a Thinkpad X230 or X220.

*as root

pkg update

pkg install sudo nano

pkg install xorg

pw groupmod video -m user || pw groupmod wheel -m user


*cp loader.conf to /boot/loader.conf

pkg install gnome3


*cp fstab to /etc/fstab

*cp rc.conf to /etc/rc.conf


pkg install drm-kmod neofetch htop alacritty

pkg install libva-intel-driver mesa-libs mesa-dri

*cp pf.conf to /etc/pf.conf

*cp remaining files to respective directories

*edit pf.conf per usage requirements

reboot

